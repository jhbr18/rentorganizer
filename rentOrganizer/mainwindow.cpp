#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //Connect to database
    di.Connect();

    //Views for Dashboard tables
    ui->tblAvailable->setModel(di.RemInventoryTableModel);
    ui->tblAvailable->show();

    ui->tblRented->setModel(di.LentItemsTableModel);
    ui->tblRented->show();


    //Views for Inventory Page
    ui->tableViewInventory->setModel(di.InventoryTableModel);
    ui->tableViewInventory->show();

    //Views for Dependencies Page
    ui->tblViewInventory->setModel(di.InventoryTableModel);
    ui->tblViewInventory->show();
    ui->tblViewDepAvailable->setModel(di.AvailableDepTableModel);
    ui->tblViewDepAvailable->show();
    ui->tblViewDependent->setModel(di.SingleDependenciesTableModel);
    ui->tblViewDependent->show();

    //View for Booking Page
    ui->tblBooking->setModel(di.PlanEventsTableModel);
    ui->tblBooking->show();

    //View for Inventory dependencies
    ui->tblDependencies->setModel(di.DependenciesTableModel);
    ui->tblDependencies->show();

    //Views for Rent planning
    ui->tblViewStock->setModel(di.RemLiveInventoryTableModel);
    ui->tblViewStock->show();

    ui->tblViewRent->setModel(di.PlanLiveInventoryTableModel);
    ui->tblViewRent->show();


    //Views for planning overview
    ui->tblViewItems->setModel(di.LentItemsTableModel);
    ui->tblViewItems->show();

    ui->tblViewRents->setModel(di.PlanEventsTableModel);
    ui->tblViewRents->show();

    updateMainPage();

    //Jump to overview Tab page
    ui->tabWidgetMain->setCurrentIndex(0);
    //Hide new order step list
    hideNewOrderSteps();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::hideNewOrderSteps()
{
    //Cleanup textboxes
    ui->tbEventLocation->setText("");
    ui->tbEventName->setText("");
    ui->tbEventOwner->setText("");
    ui->tbEventNotes->setText("");

    //Hide 2 Tabs
    ui->tabWidgetEdit->removeTab(4);
    ui->tabWidgetEdit->removeTab(4);
}

void MainWindow::on_tabWidgetMain_currentChanged(int index)
{
   updateMainPage();
}

void MainWindow::updateMainPage()
{
    switch (ui->tabWidgetMain->currentIndex()) {
    case 0:
        setToday();
        dashboardTableUpdate();
        break;
    case 1:
        resetEventView();
        break;
    case 2:
        tblInventoryUpdate();
        break;
    case 3:
        tblInventoryUpdate();
        tblReturnedUpdate();
        break;
    default:
        ui->btnSubmitBooking->setEnabled(false);
        break;
    }
}

//Dashboard----------------------------------------------------------------Dashboard

void MainWindow::on_btnToday_clicked()
{
    setToday();
}

void MainWindow::on_calendarWidget_clicked(const QDate &date)
{
    dashboardTableUpdate();
}

void MainWindow::on_calendarWidget_selectionChanged()
{
    ui->lblSelectedDate->setText(ui->calendarWidget->selectedDate().toString());
    ui->lblBestand->setText("Zum Datum verfügbar");
    ui->lblVerliehen->setText("Zum Datum entliehen");
    ui->lblSetting->setText("Geplanter Bestand");
    dashboardTableUpdate();
}

void MainWindow::on_btnRentReturn_clicked()
{
    ui->tabWidgetMain->setCurrentIndex(3);
    ui->tabWidgetEdit->setCurrentIndex(1);
}

void MainWindow::on_btnPlan_clicked()
{
    clearPlanning();
    ui->tabWidgetMain->setCurrentIndex(3);
    ui->tabWidgetEdit->setCurrentIndex(3);
}

void MainWindow::on_btnPlan_2_clicked()
{
    resetEventView();
    ui->tabWidgetMain->setCurrentIndex(1);
}


void MainWindow::on_btnViewInventory_clicked()
{
    ui->tabWidgetMain->setCurrentIndex(2);
}

void MainWindow::setToday()
{
    //ui->lblDateTime->setText( QDateTime().toString(Qt::DateFormat ) );
    //ui->lblDateTime->setText( QTime::currentTime().toString() );
    //ui->lblCurrentDate->setText("test");
    ui->calendarWidget->setSelectedDate(QDate::currentDate());
    ui->lblSelectedDate->setText(ui->calendarWidget->selectedDate().toString());
    ui->lblVerliehen->setText("Aktuell noch verliehen");
    ui->lblBestand->setText("Zurückgegeben / Verfügbar");
    ui->lblSetting->setText("Tatsächlicher Bestand");
    dashboardTableUpdate();
}

void MainWindow::dashboardTableUpdate()
{
    di.UpdateRemainingInventoryTM(ui->calendarWidget->selectedDate(),ui->calendarWidget->selectedDate(),di.RemInventoryTableModel);
    ui->tblAvailable->update();
    ui->tblAvailable->resizeColumnsToContents();

    di.UpdateLentItemsTM(ui->calendarWidget->selectedDate(),ui->calendarWidget->selectedDate());
    ui->tblRented->update();
    ui->tblRented->resizeColumnsToContents();
}

void MainWindow::clearPlanning()
{
    ui->tbEventName->setText("");
    ui->tbEventOwner->setText("");
    ui->tbEventLocation->setText("");
    ui->tbEventNotes->setText("");
    ui->cwStartDateRent->setSelectedDate(QDate::currentDate());
    di.PlanLiveInventoryTableModel->clear();
}

//tabPlanView----------------------------------------------------------------tabPlanView

void MainWindow::on_btnShowAllEvents_clicked()
{
    resetEventView();
}

void MainWindow::on_btnDeleteEvent_clicked()
{
    QModelIndexList selection = ui->tblViewRents->selectionModel()->selectedRows();
    if(selection.count() == 1)
    {
        //Only one row selected
        QModelIndex indexID = selection.at(0);
        QVariant indexNumber = di.PlanEventsTableModel->data(indexID);
        di.DeleteRentOrder(indexNumber.toString());
    }
    resetEventView();
}

void MainWindow::resetEventView()
{
    di.UpdateEventsTM();
    di.UpdateItemsSingleOrderTM("");
    ui->lblEventsTitle1->setText("alle anstehenden Events");
    ui->lblEventsTitle2->setText("alle Artikel");
    updateEventView();
    updateItemView();
}

void MainWindow::updateItemView()
{
    ui->tblViewItems->update();
    ui->tblViewItems->resizeColumnsToContents();
    //ui->btnEditEvent->setEnabled(false);
}

void MainWindow::updateEventView()
{
    ui->tblViewRents->update();
    ui->tblViewRents->resizeColumnsToContents();
    //ui->btnEditEvent->setEnabled(false);
}

void MainWindow::clickEvent(const QModelIndex &index)
{
    QAbstractItemModel *model = ui->tblViewRents->model();
    QString eventName = model->data(index.sibling(index.row(),1)).toString();
    ui->lblEventsTitle2->setText("Equipment für " + eventName);
    di.UpdateItemsSingleOrderTM(eventName);
    updateItemView();
}

void MainWindow::clickItem(const QModelIndex &index)
{
    QAbstractItemModel *model = ui->tblViewItems->model();
    QString itemName = model->data(index.sibling(index.row(),1)).toString();
    ui->lblEventsTitle1->setText("Events mit " + itemName);
    di.UpdateEventsTM(itemName);
    updateEventView();
}

void MainWindow::on_tblViewRents_doubleClicked(const QModelIndex &index)
{
    clickEvent(index);
}


void MainWindow::on_tblViewItems_doubleClicked(const QModelIndex &index)
{
    clickItem(index);
}


void MainWindow::on_tblViewRents_clicked(const QModelIndex &index)
{
    clickEvent(index);
}


void MainWindow::on_tblViewItems_clicked(const QModelIndex &index)
{
    clickItem(index);
}

//Bestand----------------------------------------------------------------Bestand

void MainWindow::on_btnAdd_clicked()
{
    ui->tabWidgetMain->setCurrentIndex(3);
    ui->tabWidgetEdit->setCurrentIndex(0);
}

void MainWindow::on_btnInventoryRefresh_clicked()
{
    tblInventoryUpdate();
}

void MainWindow::on_tableViewInventory_doubleClicked(const QModelIndex &index)
{
    ui->lblNote_2->setText("slot entered");
}

void MainWindow::tblInventoryUpdate()
{
    di.UpdateInventoryTM();
    ui->tableViewInventory->update();
    ui->tableViewInventory->resizeColumnsToContents();
    ui->tblViewInventory->update();
    ui->tblViewInventory->resizeColumnsToContents();
}


//tabAdd----------------------------------------------------------------tabAdd

void MainWindow::on_btnClear_clicked()
{
    clearEdit();
}

void MainWindow::on_btnSave_clicked()
{
    if (ui->tbPartName->text().trimmed() != "")
    {
        if(di.InsertToInventory(ui->tbPartName->text(), ui->tbPartDescription->toPlainText(), ui->tbPartQuantity->value() ))
        {
            ui->lblNote->setText("Objekt " + ui->tbPartName->text() + " wurde mit Anzahl " + ui->tbPartQuantity->cleanText() + " in die Datenbank eingefügt");
            clearEdit();
            //Update tableView
            tblInventoryUpdate();
            ui->tabWidgetMain->setCurrentIndex(2);
        }
        else
        {
            ui->lblNote->setText("Fehler beim Datenbankzugriff, bitte erneut versuchen");
        }
    }
    else
    {
        ui->lblNote->setText("Bitte einen Namen für das Objekt angeben");
    }
}

void MainWindow::clearEdit()
{
    ui->tbPartName->setText("");
    ui->tbPartQuantity->setValue(1);
    ui->tbPartDescription->clear();
}


//tabBook----------------------------------------------------------------tabBook

void MainWindow::on_btnSubmitBooking_clicked()
{
    QModelIndexList selection = ui->tblBooking->selectionModel()->selectedRows();
    if(selection.count() == 1)
    {
        //Only one row selected
        QModelIndex indexID = selection.at(0);
        di.ReturnEvent(di.PlanEventsTableModel->data(indexID).toInt());
        ui->lbltest->setText(di.PlanEventsTableModel->data(indexID).toString());
    }
    tblReturnedUpdate();
}

void MainWindow::on_tblBooking_clicked(const QModelIndex &index)
{
    ui->btnSubmitBooking->setEnabled(true);
    QModelIndexList selection = ui->tblBooking->selectionModel()->selectedRows();
    if(selection.count() == 1)
    {
        //Only one row selected
        QModelIndex indexID = selection.at(0);
        ui->lbltest->setText(di.PlanEventsTableModel->data(indexID).toString());
    }
}

void MainWindow::tblReturnedUpdate()
{
    di.ShowReturnedEventsTM();
    ui->tblBooking->update();
    ui->tblBooking->resizeColumnsToContents();
}

//tabDependencies----------------------------------------------------------------tabDependencies

void MainWindow::on_tblViewInventory_clicked(const QModelIndex &index)
{
    QModelIndexList selection = ui->tblViewInventory->selectionModel()->selectedRows();

    if(selection.count() == 1)
    {
        //Only one row selected

        QModelIndex indexID = selection.at(0);
        updateDependencies(di.InventoryTableModel->data(indexID).toString());
    }
}

void MainWindow::updateDependencies(QString inventoryIndex)
{
    di.UpdateAvailableDependenciesTM(inventoryIndex);
    ui->tblViewDepAvailable->update();
    ui->tblViewDepAvailable->resizeColumnsToContents();
    di.UpdateSingleDependenciesTM(inventoryIndex);
    ui->tblViewDependent->update();
    ui->tblViewDependent->resizeColumnsToContents();
}

void MainWindow::on_btnAddOne_2_clicked()
{
    QModelIndexList itemSelection = ui->tblViewInventory->selectionModel()->selectedRows();

    if(itemSelection.count() == 1)
    {
        //Only one item row selected
        QModelIndex indexID = itemSelection.at(0);

        QModelIndexList itemDepAvailableSelection = ui->tblViewDepAvailable->selectionModel()->selectedRows();
        if(itemDepAvailableSelection.count() == 1)
        {
            //Only one dependency available item row selected
            QModelIndex indexDepAvailableID = itemDepAvailableSelection.at(0);
            di.InsertDependency(di.InventoryTableModel->data(indexID).toString(), di.AvailableDepTableModel->data(indexDepAvailableID).toString());
            updateDependencies(di.InventoryTableModel->data(indexID).toString());
        }

    }
}

void MainWindow::on_btnDeleteOne_2_clicked()
{
    QModelIndexList itemSelection = ui->tblViewInventory->selectionModel()->selectedRows();

    if(itemSelection.count() == 1)
    {
        //Only one item row selected
        QModelIndex indexID = itemSelection.at(0);

        QModelIndexList itemDepSelection = ui->tblViewDependent->selectionModel()->selectedRows();
        if(itemDepSelection.count() == 1)
        {
            //Only one dependency available item row selected
            QModelIndex indexDepID = itemDepSelection.at(0);
            di.DeleteDependency(di.InventoryTableModel->data(indexID).toString(), di.SingleDependenciesTableModel->data(indexDepID).toString());
            updateDependencies(di.InventoryTableModel->data(indexID).toString());
        }

    }
}


//tabJob------------------------------------------------------------------tabJob

void MainWindow::on_btnSelectTime_clicked()
{
    if(ui->tbEventName->text().trimmed() == "" || ui->tbEventLocation->text().trimmed() == "" || ui->tbEventOwner->text().trimmed() == "")
        QMessageBox::critical(this, "Fehler", "Bitte geben Sie Veranstaltungsname, -ort und Ansprechpartner an!", QMessageBox::Ok);
    else
    {
        ui->tabWidgetEdit->insertTab(4, ui->tabSelectDateTime, "2. Zeitraum");
        ui->tabWidgetEdit->setCurrentIndex(4);
    }
}



//tabSelectDateTime---------------------------------------------tabSelectDateTime

void MainWindow::on_cwStartDateRent_selectionChanged()
{
    checkDateConflict();
}

void MainWindow::on_cwEndDateRent_selectionChanged()
{
    checkDateConflict();
}


void MainWindow::on_btnToday2_clicked()
{
    ui->cwStartDateRent->setSelectedDate(QDate::currentDate());
}

void MainWindow::on_btnSetReturnDate_clicked()
{
    ui->cwEndDateRent->setSelectedDate(ui->cwStartDateRent->selectedDate());
}

void MainWindow::on_btnSelectMaterial_clicked()
{
    if(!checkDateConflict())
        QMessageBox::critical(this, "Fehler", "Das Enddatum muss hinter dem Startdatum liegen.", QMessageBox::Ok);
    else
    {
        ui->tabWidgetEdit->insertTab(5, ui->tabPlan, "3. Material");
        di.PlanLiveInventoryTableModel->clear();
        di.PlanLiveInventoryTableModel->addRow("1","", "0", true, true);
        di.PlanLiveInventoryTableModel->addRow("1","", "0", true, true);
        tblLiveRemInventoryUpdate();
        tblLivePlanInventoryUpdate();
        tblDependenciesUpdate();
        ui->tabWidgetEdit->setCurrentIndex(5);
    }
}

bool MainWindow::checkDateConflict()
{
    if(ui->cwStartDateRent->selectedDate() > ui->cwEndDateRent->selectedDate())
    {
        ui->lblDateSelectNote->setText("Das Enddatum muss hinter dem Startdatum liegen.");
        return false;
    }
    else
    {
        if(ui->cwEndDateRent->selectedDate()< QDate::currentDate())
            ui->lblDateSelectNote->setText("Das Rückgabedatum sollte in der Zukunft liegen");
        else
            ui->lblDateSelectNote->setText("");

        return true;
    }
}

//tabPlan----------------------------------------------------------------tabPlan

void MainWindow::on_btnAddOne_clicked()
{
    QModelIndexList selection = ui->tblViewStock->selectionModel()->selectedRows();

    if(selection.count() == 1)
    {
        //Only one row selected

        QModelIndex indexID = selection.at(0);
        QModelIndex indexName = indexID.sibling(indexID.row(), 1);
        QString actualquantity = di.RemLiveInventoryTableModel->data(indexID.sibling(indexID.row(), 2)).toString();

        //Don't allow negative lent quantity
        if(actualquantity != "0")
        {
            di.PlanLiveInventoryTableModel->addRow(di.RemLiveInventoryTableModel->data(indexID).toString(), di.RemLiveInventoryTableModel->data(indexName).toString(), "1", true, true);
            di.RemLiveInventoryTableModel->addRow(di.RemLiveInventoryTableModel->data(indexID).toString(), di.RemLiveInventoryTableModel->data(indexName).toString(), "-1", true, false);
            tblLivePlanInventoryUpdate();
            tblDependenciesUpdate();
        }
    }
}

void MainWindow::on_btnDeleteOne_clicked()
{
    QModelIndexList selection = ui->tblViewRent->selectionModel()->selectedRows();

    if(selection.count() == 1)
    {
        //Only one row selected

        QModelIndex indexID = selection.at(0);
        QModelIndex indexName = indexID.sibling(indexID.row(), 1);

        di.RemLiveInventoryTableModel->addRow(di.PlanLiveInventoryTableModel->data(indexID).toString(), di.PlanLiveInventoryTableModel->data(indexName).toString(), "1", true, false);
        di.PlanLiveInventoryTableModel->addRow(di.PlanLiveInventoryTableModel->data(indexID).toString(), di.PlanLiveInventoryTableModel->data(indexName).toString(), "-1", true, true);
        tblLivePlanInventoryUpdate();
        tblDependenciesUpdate();
    }
}


void MainWindow::on_btnSelectCustomer_clicked()
{
    ui->tabWidgetEdit->removeTab(5);
    ui->tabWidgetEdit->removeTab(4);
    ui->tabWidgetEdit->setCurrentIndex(3);
}


void MainWindow::on_btnSelectTime_2_clicked()
{
    ui->tabWidgetEdit->removeTab(5);
    ui->tabWidgetEdit->setCurrentIndex(4);
}

void MainWindow::on_btnSubmitPlanning_clicked()
{
    if(di.PlanLiveInventoryTableModel->rowCount() > 0)
    {
        int eventId = di.CreateRentOrder(ui->tbEventName->text(), ui->tbEventLocation->text(), ui->tbEventOwner->text(), ui->tbEventNotes->toPlainText(), ui->cwStartDateRent->selectedDate(), ui->cwEndDateRent->selectedDate());
        for(int i = 0; i < di.PlanLiveInventoryTableModel->rowCount(); i++)
        {
            QModelIndex idIndex = di.PlanLiveInventoryTableModel->index(i,0);
            QModelIndex quantityIndex = di.PlanLiveInventoryTableModel->index(i,2);
            di.CreateRentItem(eventId, di.PlanLiveInventoryTableModel->data(idIndex).toInt(),di.PlanLiveInventoryTableModel->data(quantityIndex).toInt());
        }
        hideNewOrderSteps();
        ui->tabWidgetMain->setCurrentIndex(0);
    }
    else
    {
        QMessageBox::critical(this, "Fehler", "Wenigstens ein Element muss zum Verleih gehören.", QMessageBox::Ok);
    }
}

void MainWindow::tblLiveRemInventoryUpdate()
{
    di.UpdateRemainingInventoryTM(ui->cwStartDateRent->selectedDate(),ui->cwEndDateRent->selectedDate(),di.RemLiveInventoryTableModel);
    ui->tblViewStock->update();
    ui->tblViewStock->resizeColumnsToContents();
}

void MainWindow::tblLivePlanInventoryUpdate()
{
    ui->tblViewRent->update();
    ui->tblViewRent->resizeColumnsToContents();
}

void MainWindow::tblDependenciesUpdate()
{
    di.UpdateDependenciesTM(di.PlanLiveInventoryTableModel->getAllIDs());
    ui->tblDependencies->update();
    ui->tblDependencies->resizeColumnsToContents();
}


//---------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------


