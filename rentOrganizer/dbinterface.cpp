#include "dbinterface.h"

DbInterface::DbInterface()
{

}

bool DbInterface::Connect()
{
    //Get path of sqlLite Database
    QString dbPath = QDir::currentPath();
    dbPath = dbPath.left(dbPath.lastIndexOf('/'));
    dbPath += "/db";

    //Open Database
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(dbPath
                        + QDir::separator()
                        + "rentOrganizer.db" );
    if (!db.open())
    {
        //db.lastError().text();
        return false;
    }

    //Initialize model for inventory table
    InventoryTableModel = new QSqlTableModel(nullptr, db);

    //Initialize model for lent items
    LentItemsTableModel = new QSqlQueryModel;

    //Initialize table model for remaining items
    RemInventoryTableModel = new ManualInventoryModel;

    //Initialize table model for remaining items with option for live quantity changing
    RemLiveInventoryTableModel = new ManualInventoryModel;

    //Initialize table model for planned items with option for live quantity changing
    PlanLiveInventoryTableModel = new ManualInventoryModel;

    //Initialize table model to show all planned Events in the Future
    PlanEventsTableModel = new QSqlQueryModel;

    //Initialize table model for inventory dependencies
    DependenciesTableModel = new QSqlQueryModel;

    //Initialize table model for available dependencies
    AvailableDepTableModel = new QSqlQueryModel;

    //Initialize table model for single inventory item dependencies
    SingleDependenciesTableModel = new QSqlQueryModel;

    return true;
}

bool DbInterface::InsertToInventory(const QString& name, const QString& description, const
int& quantity)
{
    QSqlQuery query(db);
    query.prepare("INSERT INTO Inventory (Name, Description, quantity) "
    "VALUES (:name, :description ,:quantity)");
    query.bindValue(":name", name);
    query.bindValue(":description", description);
    query.bindValue(":quantity", quantity);
    if (!query.exec())
    {
        //db.lastError().text();
        return false;
    }

    return true;
}

bool DbInterface::UpdateInventoryTM()
{
    InventoryTableModel->clear();
    InventoryTableModel->setTable("Inventory");
    InventoryTableModel->select();
    InventoryTableModel->setHeaderData(0, Qt::Horizontal, "ID");
    InventoryTableModel->setHeaderData(1, Qt::Horizontal, "Name");
    InventoryTableModel->setHeaderData(2, Qt::Horizontal, "Beschreibung");
    InventoryTableModel->setHeaderData(3, Qt::Horizontal, "Anzahl");

    return true;
}


bool DbInterface::UpdateRemainingInventoryTM(QDate startDate, QDate endDate, ManualInventoryModel* tableView)
{
    //Subtrahieren von geliehenen Komponenten in QT weil Sqllite kein FULL outer join unterstützt

    QString sStartDate = startDate.toString("yyyy-MM-dd");
    QString sEndDate = endDate.toString("yyyy-MM-dd");

    tableView->clear();

    QSqlQueryModel tmpLentItems;
    tmpLentItems.setQuery("SELECT RentItems.ItemInventoryIndex, SUM(RentItems.Quantity) AS QuantitySum FROM RentOrders JOIN RentItems ON RentOrders.\"Index\"=RentItems.RentOrderIndex WHERE (RentOrders.beginDate <= '" + sStartDate + "' AND RentOrders.endDate >= '" + sStartDate + "') OR (RentOrders.beginDate >= '" + sStartDate + "' AND RentOrders.beginDate <= '" + sEndDate + "') GROUP BY RentItems.ItemInventoryIndex", db);

    QSqlQueryModel tmpInventoryModel;
    tmpInventoryModel.setQuery("SELECT \"Index\", Name, Quantity FROM Inventory");

    int inventoryCount = tmpInventoryModel.rowCount();
    int lentItemsCount = tmpLentItems.rowCount();

    for(int i = 0; i < inventoryCount; i++)
    {
        int currentInvID = tmpInventoryModel.data(tmpInventoryModel.index(i, 0)).toInt();
        int currentInvCount = tmpInventoryModel.data(tmpInventoryModel.index(i, 2)).toInt();

        for(int u = 0; u < lentItemsCount; u++)
        {
            if(tmpLentItems.data(tmpLentItems.index(u, 0)).toInt() == currentInvID)
            {
                currentInvCount -= tmpLentItems.data(tmpLentItems.index(u, 1)).toInt();

                //Do not allow negative quantities
                if(currentInvCount < 0)
                    currentInvCount = 0;

                break;
            }
        }
        tableView->addRow(tmpInventoryModel.data(tmpInventoryModel.index(i, 0)).toString(), tmpInventoryModel.data(tmpInventoryModel.index(i, 1)).toString(), QString::number(currentInvCount), false, false);
    }

    return true;
}

bool DbInterface::UpdateLentItemsTM(QDate startDate, QDate endDate)
{
    QString sStartDate = startDate.toString("yyyy-MM-dd");
    QString sEndDate = endDate.toString("yyyy-MM-dd");

    LentItemsTableModel->clear();
    LentItemsTableModel->setQuery("SELECT RentItems.ItemInventoryIndex, Inventory.Name, SUM(RentItems.Quantity) AS QuantitySum FROM RentOrders JOIN RentItems ON RentOrders.\"Index\"=RentItems.RentOrderIndex JOIN Inventory ON Inventory.\"Index\"=RentItems.ItemInventoryIndex WHERE (RentOrders.beginDate <= '" + sStartDate + "' AND RentOrders.endDate >= '" + sStartDate + "') OR (RentOrders.beginDate >= '" + sStartDate + "' AND RentOrders.beginDate <= '" + sEndDate + "') GROUP BY RentItems.ItemInventoryIndex", db);
    LentItemsTableModel->setHeaderData(0, Qt::Horizontal, "ID");
    LentItemsTableModel->setHeaderData(1, Qt::Horizontal, "Name");
    LentItemsTableModel->setHeaderData(2, Qt::Horizontal, "Anzahl");

    return true;
}

bool DbInterface::UpdateAvailableDependenciesTM(QString inventoryIndex)
{
    AvailableDepTableModel->clear();
    AvailableDepTableModel->setQuery("SELECT Inventory.'Index', Inventory.name FROM Inventory WHERE NOT Inventory.'Index' = '" + inventoryIndex + "' AND Inventory.'Index' NOT IN (SELECT InventoryDependencies.DepItemInventoryIndex FROM InventoryDependencies WHERE InventoryDependencies.ItemInventoryIndex = '" + inventoryIndex + "')", db);
    AvailableDepTableModel->setHeaderData(0, Qt::Horizontal, "ID");
    AvailableDepTableModel->setHeaderData(1, Qt::Horizontal, "Name");

    return true;
}

bool DbInterface::UpdateSingleDependenciesTM(QString inventoryIndex)
{
    SingleDependenciesTableModel->clear();
    SingleDependenciesTableModel->setQuery("SELECT Inventory.'Index', Inventory.name FROM InventoryDependencies JOIN Inventory ON InventoryDependencies.DepItemInventoryIndex = Inventory.'Index' WHERE InventoryDependencies.ItemInventoryIndex = '" + inventoryIndex + "'", db);
    SingleDependenciesTableModel->setHeaderData(0, Qt::Horizontal, "ID");
    SingleDependenciesTableModel->setHeaderData(1, Qt::Horizontal, "Name");

    return true;
}

bool DbInterface::UpdateItemsSingleOrderTM(const QString& event)
{
    //QString sRentOrderIndex = QString::number(rentOrderIndex);
    LentItemsTableModel->clear();
    if(event != "") LentItemsTableModel->setQuery("SELECT RentItems.ItemInventoryIndex, Inventory.Name, RentItems.Quantity FROM RentOrders JOIN RentItems ON RentOrders.\"Index\"=RentItems.RentOrderIndex JOIN Inventory ON Inventory.\"Index\"=RentItems.ItemInventoryIndex WHERE RentOrders.\"Name\" = '" + event + "' GROUP BY RentItems.ItemInventoryIndex", db);
    else            LentItemsTableModel->setQuery("SELECT Inventory.\"Index\", Inventory.Name, Inventory.Quantity FROM Inventory", db);
    LentItemsTableModel->setHeaderData(0, Qt::Horizontal, "ID");
    LentItemsTableModel->setHeaderData(1, Qt::Horizontal, "Name");
    LentItemsTableModel->setHeaderData(2, Qt::Horizontal, "Anzahl");

    return true;
}

bool DbInterface::UpdateEventsTM()
{
    return UpdateEventsTM("", QDate::currentDate());
}

bool DbInterface::UpdateEventsTM(QDate date)
{
    return UpdateEventsTM("", date);
}

bool DbInterface::UpdateEventsTM(const QString &item)
{
    return UpdateEventsTM(item, QDate::currentDate());
}

bool DbInterface::UpdateEventsTM(const QString &item, QDate date)
{
    QString sdate = date.toString("yyyy-MM-dd");
    QString query;

    PlanEventsTableModel->clear();
    if(item != "")  query = "SELECT RentOrders.\"Index\", RentOrders.Name, beginDate, endDate, RentItems.Quantity FROM RentOrders JOIN RentItems ON RentOrders.\"Index\" = RentItems.RentOrderIndex JOIN Inventory ON Inventory.\"Index\"=RentItems.ItemInventoryIndex WHERE Inventory.\"Name\" = '" + item + "' GROUP BY RentItems.RentOrderIndex";
    else
    {
        query = "SELECT RentOrders.\"Index\", Name, beginDate, endDate FROM RentOrders WHERE ";
        if (sdate != (QDate::currentDate().toString("yyyy-MM-dd")))
            query += "RentOrders.beginDate <= '" + sdate + "' AND RentOrders.endDate >= '" + sdate + "'";
        else
            query += "RentOrders.endDate >= '" + sdate + "'";
    }
    query += " ORDER BY beginDate";

    PlanEventsTableModel->setQuery(query, db);
    PlanEventsTableModel->setHeaderData(0, Qt::Horizontal, "Nummer");
    PlanEventsTableModel->setHeaderData(1, Qt::Horizontal, "Name");
    PlanEventsTableModel->setHeaderData(2, Qt::Horizontal, "Beginn");
    PlanEventsTableModel->setHeaderData(3, Qt::Horizontal, "Ende");
    if(item != "") PlanEventsTableModel->setHeaderData(4, Qt::Horizontal, "Anzahl");
    return true;
}

bool DbInterface::ShowReturnedEventsTM()
{
    QString sdate = QDate::currentDate().toString("yyyy-MM-dd");
    QString query;
    PlanEventsTableModel->clear();
    query = "SELECT RentOrders.\"Index\", Name, Location, beginDate, endDate FROM RentOrders WHERE RentOrders.endDate <= '" + sdate + "' AND RentOrders.returned = 'FALSE' ORDER BY beginDate";

    PlanEventsTableModel->setQuery(query, db);
    PlanEventsTableModel->setHeaderData(0, Qt::Horizontal, "Nummer");
    PlanEventsTableModel->setHeaderData(1, Qt::Horizontal, "Name");
    PlanEventsTableModel->setHeaderData(2, Qt::Horizontal, "Location");
    PlanEventsTableModel->setHeaderData(3, Qt::Horizontal, "Beginn");
    PlanEventsTableModel->setHeaderData(4, Qt::Horizontal, "Ende");
    return true;
}

bool DbInterface::ReturnEvent(const int &rentOrderIndex)
{
    QVariant var(rentOrderIndex);
    QString stringValue = var.toString();
    QSqlQuery query(db);
    query.prepare("UPDATE RentOrders SET returned = 'TRUE' WHERE RentOrders.\"Index\" = " + stringValue);
    if (!query.exec())
    {
        return false;
    }
    return true;
}

bool DbInterface::UpdateDependenciesTM(QList<QString> inventoryIndizes)
{
    int invIndizesCount = inventoryIndizes.count();

    DependenciesTableModel->clear();
    if(invIndizesCount > 0)
    {
        QString query = "SELECT Inventory.'Index', Inventory.name, i1.Name as \"benötigt von\" FROM Inventory i1 JOIN InventoryDependencies ON i1.'Index' = InventoryDependencies.ItemInventoryIndex JOIN Inventory ON InventoryDependencies.DepItemInventoryIndex = Inventory.'Index' WHERE ";

        for(int i = 0; i < invIndizesCount; i++)
        {
            query += "i1.'Index' = " + inventoryIndizes.at(i);

            if(i < (invIndizesCount - 1))
                query += " OR ";
        }

        DependenciesTableModel->setQuery(query, db);
        DependenciesTableModel->setHeaderData(0, Qt::Horizontal, "ID");
        DependenciesTableModel->setHeaderData(1, Qt::Horizontal, "Name");
        DependenciesTableModel->setHeaderData(2, Qt::Horizontal, "benötigt von");
    }

    return true;
}

bool DbInterface::InsertDependency(QString inventoryIndex, QString inventoryDepIndex)
{
    QSqlQuery query(db);
    query.prepare("INSERT INTO InventoryDependencies (ItemInventoryIndex, DepItemInventoryIndex) "
    "VALUES (:invIndex, :invDepIndex)");
    query.bindValue(":invIndex", inventoryIndex);
    query.bindValue(":invDepIndex", inventoryDepIndex);
    if (!query.exec())
    {
        //db.lastError().text();
        return false;
    }

    return true;
}

bool DbInterface::DeleteDependency(QString inventoryIndex, QString inventoryDepIndex)
{
    QSqlQuery query(db);
    query.prepare("DELETE FROM InventoryDependencies WHERE ItemInventoryIndex = :invIndex AND DepItemInventoryIndex = :invDepIndex");
    query.bindValue(":invIndex", inventoryIndex);
    query.bindValue(":invDepIndex", inventoryDepIndex);
    if (!query.exec())
    {
        //db.lastError().text();
        return false;
    }

    return true;
}

bool DbInterface::DeleteRentOrder(QString rentOrderIndex)
{
    QSqlQuery query(db);
    query.prepare("DELETE FROM RentOrders WHERE RentOrders.\"Index\" = :RentIndex");
    query.bindValue(":RentIndex", rentOrderIndex);
    if (!query.exec()) return false;
    return true;
}

int DbInterface::CreateRentOrder(const QString &name, const QString &location, const QString &contact, const QString &notes, QDate beginDate, QDate endDate)
{
    QString sBeginDate = beginDate.toString("yyyy-MM-dd");
    QString sEndDate = endDate.toString("yyyy-MM-dd");

    QSqlQuery query(db);
    query.prepare("INSERT INTO RentOrders (Name, beginDate, endDate, Location, Contact, Notes, returned) "
    "VALUES (:name, :beginDate, :endDate, :location, :contact, :notes, :returned)");
    query.bindValue(":name", name);
    query.bindValue(":beginDate", sBeginDate);
    query.bindValue(":endDate", sEndDate);
    query.bindValue(":location", location);
    query.bindValue(":contact", contact);
    query.bindValue(":notes", notes);
    query.bindValue(":returned", "FALSE");
    if (!query.exec())
    {
        //ToDo: Handle errors
    }

    //Get ID of created event
    QSqlQuery queryMaxIndex(db);
    queryMaxIndex.prepare("SELECT MAX(\"Index\") FROM RentOrders");
    if (!queryMaxIndex.exec()) {
        //ToDo: Handle errors
    }
    queryMaxIndex.next();

    return queryMaxIndex.value(0).toInt();
}

bool DbInterface::CreateRentItem(const int &rentOrderIndex, const int &itemInventoryIndex, const int &quantity)
{
    QSqlQuery query(db);
    query.prepare("INSERT INTO RentItems (RentOrderIndex, ItemInventoryIndex, Quantity) "
    "VALUES (:rentOrderIndex, :itemInventoryIndex, :quantity)");
    query.bindValue(":rentOrderIndex", rentOrderIndex);
    query.bindValue(":itemInventoryIndex", itemInventoryIndex);
    query.bindValue(":quantity", quantity);
    if (!query.exec())
    {
        return false;
    }
    return true;
}
