#include "manualinventorymodel.h"

ManualInventoryModel::ManualInventoryModel(QObject *parent) : QAbstractTableModel(parent)
{

}

void ManualInventoryModel::addRow(const QString &s_id, const QString &s_name, const QString &s_quantity, bool mergeQuantityOnIndex, bool deleteNullQuantityItems)
{
   bool createNewRow = false;

   if(mergeQuantityOnIndex)
   {
      //Only correct quantity on existing item
       int rowIndex = tm_id.indexOf(s_id);

       if(rowIndex < 0)
           createNewRow=true;
       else
       {
           int currentQuantity = tm_quantity.at(rowIndex).toInt();
           int quantityDelta = s_quantity.toInt();
           int newQuantity = currentQuantity + quantityDelta;
           if(newQuantity < 0)
               newQuantity = 0;

           if(newQuantity == 0 && deleteNullQuantityItems)
           {
               //RemoveRow
               tm_id.removeAt(rowIndex);
               tm_name.removeAt(rowIndex);
               tm_quantity.removeAt(rowIndex);
           }
           else
           {
               tm_quantity.replace(tm_id.indexOf(s_id), QString::number(newQuantity));
           }
       }
   }

   if(createNewRow || !mergeQuantityOnIndex)
   {
       tm_id.append(s_id);
       tm_name.append(s_name);
       tm_quantity.append(s_quantity);
   }

   emit layoutChanged();
}

void ManualInventoryModel::clear()
{
    tm_id.clear();
    tm_name.clear();
    tm_quantity.clear();
}

int ManualInventoryModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return tm_id.length();
}

int ManualInventoryModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 3;
}

QVariant ManualInventoryModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || role != Qt::DisplayRole) {
        return QVariant();
    }
    if (index.column() == 0) {
        return tm_id[index.row()];
    } else if (index.column() == 1) {
        return tm_name[index.row()];
    }
    else if (index.column() == 2) {
        return tm_quantity[index.row()];
    }
    return QVariant();
}

QVariant ManualInventoryModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        if (section == 0) {
            return QString("ID");
        } else if (section == 1) {
            return QString("Name");
        }
        else if (section == 2) {
            return QString("Anzahl");
        }
    }
    return QVariant();
}

QList<QString> ManualInventoryModel::getAllIDs()
{
    return tm_id;
}
