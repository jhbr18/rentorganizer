#ifndef DBINTERFACE_H
#define DBINTERFACE_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDir>
#include <QSqlTableModel>

#include "manualinventorymodel.h"

class DbInterface
{
private:
    QSqlDatabase db;
public:

    DbInterface();

    bool Connect();

    /**
     * @brief InsertToInventory: Inserting one row into the "Inventory" database table using the parameters as values
     * @param name - Name of Item
     * @param description - short description of the Item to add
     * @param quantity - Item quantity
     * @return confirmation of successful SQL query
     */
    bool InsertToInventory(const QString& name, const QString& description, const int& quantity);

    /**
     * @brief UpdateInventoryTM: reloads content of "Inventory" Database Table into InventoryTableModel
     * @return confirmation of successful SQL query
     */
    bool UpdateInventoryTM();

    /**
     * @brief UpdateEventsTM: calls UpdateEventsTM with empty Item and current Date
     * @return confirmation of successful SQL query
     */
    bool UpdateEventsTM();

    /**
     * @brief UpdateEventsTM: calls UpdateEventsTM with empty Item and Date parameter
     * @param date
     * @return confirmation of successful SQL query
     */
    bool UpdateEventsTM(QDate date);

    /**
     * @brief UpdateEventsTM: calls UpdateEventsTM with current Date and Item parameter
     * @param item
     * @return confirmation of successful SQL query
     */
    bool UpdateEventsTM(const QString& item);

    /**
     * @brief UpdateEventsTM: updates PlanEventsTableModel to show Events matching the parameters
     * @param item - If Item parameter is not Empty, all Rent Orders containing this Item will be shown.
     * @param date - If Item parameter is Empty, all Rent Orders with End Date following the date parameter are shown
     * @return confirmation of successful SQL query
     */
    bool UpdateEventsTM(const QString& item, QDate date);

    /**
     * @brief ShowReturnedEventsTM: updates PlanEventsTableModel to show all Rent orders, where the 'returned' row displays 'FALSE'
     * @return confirmation of successful SQL query
     */
    bool ShowReturnedEventsTM();

    /**
     * @brief ReturnEvent: writes 'TRUE' into the database Table 'RentOrders' in row 'returned'
     * @param rentOrderIndex
     * @return confirmation of successful SQL query
     */
    bool ReturnEvent(const int& rentOrderIndex);

    /**
     * @brief UpdateRemainingInventoryTM: this Function will fill the referenced Table Modell with all Inventory Items not assigned to a Rent Order within the given Timespan
     * @param startDate - begin of the Timespan
     * @param endDate - end of the Timespan
     * @param tableView - referenced table to update
     * @return confirmation of successful SQL query
     */
    bool UpdateRemainingInventoryTM(QDate startDate, QDate endDate, ManualInventoryModel* tableView);

    /**
     * @brief UpdateLentItemsTM: updates LentItemsTableModel to show all Items involved in a rent order within the time span
     * @param startDate - begin of the time span
     * @param endDate - end of the time span
     * @return confirmation of successful SQL query
     */
    bool UpdateLentItemsTM(QDate startDate, QDate endDate);

    /**
     * @brief UpdateItemsSingleOrderTM: updates LentItemsTableModel to show the Rent List of a single Event, or all Items in the Inventory Table, if event parameter is empty
     * @param event - If not empty, Items of this Rent Order will be shown
     * @return confirmation of successful SQL query
     */
    bool UpdateItemsSingleOrderTM(const QString& event);

    /**
     * @brief UpdateDependenciesTM: updates DependenciesTableModel to show all connected Dependencies Items connected to this Item Index List
     * @param inventoryIndizes - List of Indizes to show Dependencies of
     * @return confirmation of successful SQL query
     */
    bool UpdateDependenciesTM(QList<QString> inventoryIndizes);

    /**
     * @brief UpdateSingleDependenciesTM: updates DependenciesTableModel to show all connected Dependencies Items connected to this Item Index
     * @param inventoryIndex - Index of the Item to which the dependencies should be shown
     * @return confirmation of successful SQL query
     */
    bool UpdateSingleDependenciesTM(QString inventoryIndex);

    /**
     * @brief UpdateAvailableDependenciesTM: updates AvailableDepTableModel to show all Items, that are not assigned to depend on the Item with given Index
     * @param inventoryIndex - Index of the Item to show all Items not depending on
     * @return confirmation of successful SQL query
     */
    bool UpdateAvailableDependenciesTM(QString inventoryIndex);

    /**
     * @brief InsertDependency: Inserts a row in the InventoryDependencies Table with the given parameters
     * @param inventoryIndex - Index of the Item to depend on
     * @param inventoryDepIndex - Index of the Item depending on the first Item
     * @return confirmation of successful SQL query
     */
    bool InsertDependency(QString inventoryIndex, QString inventoryDepIndex);

    /**
     * @brief DeleteDependency: Deletes a single row from the InventoryDependencies table matching both the depending and dependent Items
     * @param inventoryIndex - Index of the Item to depend on
     * @param inventoryDepIndex - Index of the Item depending on the first Item
     * @return confirmation of successful SQL query
     */
    bool DeleteDependency(QString inventoryIndex, QString inventoryDepIndex);

    /**
     * @brief DeleteRentOrder: Deletes a single row from the RentOrders table with given Index
     * @param rentOrderIndex - Index of the RentOrder to be deleted
     * @return confirmation of successful SQL query
     */
    bool DeleteRentOrder(QString rentOrderIndex);

    /**
     * @brief CreateRentOrder: adds a new Row in the RentOrders Table with the given parameters as row values
     * @param name
     * @param location
     * @param contact
     * @param notes
     * @param beginDate
     * @param endDate
     * @return the Index of the new created RentOrder
     */
    int CreateRentOrder(const QString& name, const QString& location, const QString& contact, const QString& notes, QDate beginDate, QDate endDate);

    /**
     * @brief CreateRentItem: Adds a single Item to a rent Order
     * @param rentOrderIndex - Rent order Index to add Item to
     * @param itemInventoryIndex - Index of the Item to be added to the Rent Order
     * @param quantity - number of Items used of this type in this order
     * @return confirmation of successful SQL query
     */
    bool CreateRentItem(const int& rentOrderIndex, const int& itemInventoryIndex, const int& quantity);

    QSqlTableModel *InventoryTableModel;
    QSqlQueryModel *LentItemsTableModel;
    ManualInventoryModel *RemInventoryTableModel;
    ManualInventoryModel *RemLiveInventoryTableModel;
    ManualInventoryModel *PlanLiveInventoryTableModel;
    QSqlQueryModel *PlanEventsTableModel;
    QSqlQueryModel *DependenciesTableModel;
    QSqlQueryModel *AvailableDepTableModel;
    QSqlQueryModel *SingleDependenciesTableModel;
};

#endif // DBINTERFACE_H
