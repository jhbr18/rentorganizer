#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDateTime>
#include <QMessageBox>
#include "dbinterface.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btnToday_clicked();

    void on_btnSave_clicked();

    void on_btnClear_clicked();

    void clearEdit();

    void setToday();

    void on_calendarWidget_selectionChanged();

    void on_tableViewInventory_doubleClicked(const QModelIndex &index);

    void on_btnSubmitBooking_clicked();

    void on_btnRentReturn_clicked();

    void on_tblBooking_clicked(const QModelIndex &index);

    void on_tabWidgetMain_currentChanged(int index);

    void on_btnPlan_clicked();

    void on_btnSubmitPlanning_clicked();

    void on_btnInventoryRefresh_clicked();

    void on_btnSelectTime_clicked();

    void on_btnSelectMaterial_clicked();

    void on_btnSelectTime_2_clicked();

    void on_btnSelectCustomer_clicked();

    void on_btnAddOne_clicked();

    void on_btnDeleteOne_clicked();

    void on_btnAdd_clicked();

    void on_cwStartDateRent_selectionChanged();

    void on_cwEndDateRent_selectionChanged();

    void on_btnSetReturnDate_clicked();

    void on_btnToday2_clicked();

    void on_btnPlan_2_clicked();

    void on_tblViewRents_doubleClicked(const QModelIndex &index);

    void on_tblViewItems_doubleClicked(const QModelIndex &index);

    void on_btnShowAllEvents_clicked();

    void on_calendarWidget_clicked(const QDate &date);

    void on_btnAddOne_2_clicked();

    void on_btnDeleteOne_2_clicked();

    void on_tblViewInventory_clicked(const QModelIndex &index);

    void tblReturnedUpdate();

    void on_btnDeleteEvent_clicked();

    void on_btnViewInventory_clicked();

    void on_tblViewRents_clicked(const QModelIndex &index);

    void on_tblViewItems_clicked(const QModelIndex &index);

private:
    Ui::MainWindow *ui;
    DbInterface di;

    /**
     * @brief tblInventoryUpdate: updates the Inventory Tables inside the "Bestand" Tab and the "Bearbeiten->Abhängigkeiten" Tab
     */
    void tblInventoryUpdate();

    /**
     * @brief tblLiveRemInventoryUpdate: updates the "Bestand" table on the "Bearbeiten->Material" page
     */
    void tblLiveRemInventoryUpdate();

    /**
     * @brief tblLivePlanInventoryUpdate: updates the "Verleihliste" table on the "Bearbeiten->Material" page
     */
    void tblLivePlanInventoryUpdate();

    /**
     * @brief tblDependenciesUpdate: updates the "Abhängigkeiten" table on the "Bearbeiten->Material" page
     */
    void tblDependenciesUpdate();

    /**
     * @brief dashboardTableUpdate: updates both tables in the "Übersicht" Tab
     */
    void dashboardTableUpdate();

    /**
     * @brief clearPlanning: deletes all inputs made to create an event
     */
    void clearPlanning();

    /**
     * @brief checkDateConflict: Checks if event return date is after event start date and return date is in the future
     * @return no date conflict present
     */
    bool checkDateConflict();

    /**
     * @brief resetEventView: updates both tables in the "Events" Tab using the single update functions
     */
    void resetEventView();

    /**
     * @brief updateEventView: updates the left table in the "Events" Tab
     */
    void updateEventView();

    /**
     * @brief updateDependencies: updates both lower tables in the "Bearbeiten->Abhängigkeiten" Tab
     * @param inventoryIndex: Item Inventory Index to show Dependencies to
     */
    void updateDependencies(QString inventoryIndex);

    /**
     * @brief updateItemView: updates the right table in the "Events" Tab
     */
    void updateItemView();

    /**
     * @brief updateMainPage: function to update the tables at changing tabs
     */
    void updateMainPage();

    /**
     * @brief clickEvent: click into the Events table in the "Events" Tab
     * @param index: clicked Index
     */
    void clickEvent(const QModelIndex &index);

    /**
     * @brief clickItem: click into the Items table in the "Events" Tab
     * @param index: clicked Index
     */
    void clickItem(const QModelIndex &index);

    /**
     * @brief hideNewOrderSteps: hides the 2.(Zeitraum) and 3.(Material) step of creating a new Event
     */
    void hideNewOrderSteps();

};
#endif // MAINWINDOW_H
