#ifndef MANUALINVENTORYMODEL_H
#define MANUALINVENTORYMODEL_H

#include <QObject>
#include <QString>
#include <QAbstractTableModel>

class ManualInventoryModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    ManualInventoryModel(QObject *parent = 0);

    void addRow(const QString &s_id, const QString &s_name, const QString &s_quantity, bool mergeQuantityOnIndex, bool deleteNullQuantityItems);
    void clear();

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

    QList<QString> getAllIDs();

private:
    QList<QString> tm_id;
    QList<QString> tm_name;
    QList<QString> tm_quantity;
};

#endif // MANUALINVENTORYMODEL_H
