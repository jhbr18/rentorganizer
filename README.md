# RentOrganizer

## Allgemeines

Tool zum Organisieren eines Verleih-Bestandes (z.B. im Bereich der Veranstaltungstechnik)

## Anforderungen an das Projekt

**Datenbank**

    Datenbank anlegen
    Ansprechen der Datenbank mit Qt
    Datenbankmodell entwerfen (Normalisierung beachten)
    Tabellen anlegen und prüfen

**GUI**

    SQLlite als DB nutzen
    Formular für Komponente anlegen
    Komponente(n) verleihen
    Komponente(n) zurückgeben
    (Tabellarische) Komponentenübersicht
    Filterfunktionen
    Abhängigkeiten anzeigen
    Editor für zusammenstellungen / Verleihlisten (Warnung bei doppelbelegung)
    (optional) Kalenderansicht für Verleihungen
    Dashboard geteilt in "Verliehen/Rückgabe" und "Kalender und Verleih"
    Warnungen für Verzug, Übersicht der entliehenen Komponenten, (Kalenderübersicht)
